using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Tests.Helper;
using Tests.Helpers;

namespace Tests
{
    public class Tests
    {
        private IWebDriver driver;

        [SetUp]
        public void SetupTest()
        {
            
        }
        [TearDown]
        public void TeardownTest()
        {
            
        }

        public static string RandomText()
        {
            Random random = new Random();
            int length = 10;
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        [Test]
        public void Connect1()
        {
            //MetaMask
            driver = Constants.Driver;
            driver.Manage().Window.Maximize();
            NavigateTo.Page("chrome-extension://nkbihfbeogaeaoehlefnkodbefgpgknn/home.html#initialize/welcome", driver);

            Thread.Sleep(Constants.LongTimeout);

            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("window.open();");
            IList<string> tabs = new List<string>(driver.WindowHandles);
            driver.SwitchTo().Window(tabs[1]);

            Actions.ClickByXPath("//button[@class='button btn--rounded btn-primary first-time-flow__button']", driver);//Start work
            Actions.ClickByXPath("//*[@id='app-content']/div/div[2]/div/div/div[2]/div/div[2]/div[2]/button", driver);//New
            Actions.ClickByXPath("//button[@class='button btn--rounded btn-primary page-container__footer-button']", driver);//Agree

            Actions.SetTextById("create-password", "123456789", driver);
            Actions.SetTextById("confirm-password", "123456789", driver);
            Actions.ClickByXPath("//*[@id='app-content']/div/div[2]/div/div/div[2]/form/div[3]/div", driver);//checkbox
            Thread.Sleep(Constants.LongTimeout);
            Actions.ClickByXPath("//button[@class='button btn--rounded btn-primary first-time-flow__button']", driver);//create

            Thread.Sleep(Constants.LongTimeout);
            Actions.ClickByXPath("//button[@class='button btn--rounded btn-primary']", driver);//next
            Actions.ClickByXPath("//button[@class='button btn--rounded btn-secondary first-time-flow__button']", driver);//later

            Thread.Sleep(Constants.LongTimeout);

            //flagnetwork
            NavigateTo.Page("https://dashboard.flagnetwork.finance/airdrop?claimRef=0xF6CB5e6435E8aF4ced75529FEecf61fF01ef15FA", driver);

            Thread.Sleep(Constants.LongTimeout);
            Actions.ClickByXPath("//header[@class='ant-layout-header header-layout']/div[2]/button", driver);//connect
            Actions.ClickByXPath("//button[@class='btn_login-metamask']", driver);//metamask

            Thread.Sleep(Constants.LongTimeout);
            IJavaScriptExecutor js2 = (IJavaScriptExecutor)driver;
            js2.ExecuteScript("window.open();");
            IList<string> tabs2 = new List<string>(driver.WindowHandles);
            driver.SwitchTo().Window(tabs2[4]);

            Actions.ClickByXPath("//button[@class='button btn--rounded btn-primary']", driver);//next
            Actions.ClickByXPath("//button[@class='button btn--rounded btn-primary page-container__footer-button']", driver);//connect
            Actions.ClickByXPath("//button[@class='button btn--rounded btn-primary']", driver);//agree

            IJavaScriptExecutor js3 = (IJavaScriptExecutor)driver;
            js3.ExecuteScript("window.open();");
            IList<string> tabs3 = new List<string>(driver.WindowHandles);

            driver.FindElement(By.XPath("//button[@class='button btn--rounded btn-primary']")).Click();

            driver.SwitchTo().Window(tabs3[1]);

            Thread.Sleep(Constants.LongTimeout);

            Actions.ClickByXPath("//ul[@class='balance-widget']/li[1]/div", driver);//telegram1
            Actions.ClickByXPath("//ul[@class='balance-widget']/li[2]/div", driver);//telegram2
            Actions.ClickByXPath("//ul[@class='balance-widget']/li[3]/div", driver);//twitter1
            Actions.ClickByXPath("//ul[@class='balance-widget']/li[4]/div", driver);//twitter2

            IJavaScriptExecutor js4 = (IJavaScriptExecutor)driver;
            js4.ExecuteScript("window.open();");
            IList<string> tabs4 = new List<string>(driver.WindowHandles);
            driver.SwitchTo().Window(tabs4[1]);

            Thread.Sleep(Constants.LongTimeout);
            Actions.ClickByXPath("//ul[@class='balance-widget']/li[5]/div/button", driver);//submit information

            Actions.SetTextById("twitterId", RandomText(), driver);
            Actions.SetTextById("telegramId", RandomText(), driver);
            Actions.ClickByXPath("//div[@class='modal-dialog modal-dialog-centered modal-dialog-scrollable']/div/div[2]/div/div/form/div[3]/button", driver);//submit
            Thread.Sleep(Constants.LongTimeout);
            driver.Quit();
        }

        [Test]
        public void ConnectN()
        {
            for (int i = 0; i < Constants.N; i++)
                Connect1();
        }
    }
}