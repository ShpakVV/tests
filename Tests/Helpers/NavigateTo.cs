﻿using System;
using OpenQA.Selenium;
using Tests.Helpers;

namespace Tests.Helper
{
    public class NavigateTo
    {
        public static void Page(string page, IWebDriver driver)
        {
            driver.Navigate().GoToUrl(page);

            var wait = new OpenQA.Selenium.Support.UI.WebDriverWait(driver, TimeSpan.FromSeconds(Constants.Timeout1));
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            WaitElement.FullLoad(driver);
        }
    }
}
