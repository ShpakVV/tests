﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Tests.Helpers;

namespace Tests.Helper
{
    public class WaitElement
    {
        public static bool WaitExactlyCount(string element, int count, IWebDriver driver)
        {
            FindBy.XPath(element, driver);
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(Constants.Timeout1));
                wait.Until(drv => drv.FindElements(By.XPath(element)).Count == count);
                Thread.Sleep(Constants.ShortTimeout);
            }
            catch (Exception)
            { }
            if (driver.FindElements(By.XPath(element)).Count == count) return true;
            else return false;
        }

        public static void WaitUrlContains(string text, IWebDriver driver)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(Constants.TimeoutForTimeSpan));
                wait.Until(drv => drv.Url.Contains(text));
                Thread.Sleep(Constants.ShortTimeout);
            }
            catch (Exception)
            { throw new Exception(text + " doesn't contains in " + driver.Url); }
        }
        public static void WaitText(string text, IWebDriver driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(Constants.Timeout1));
            wait.Until(drv => drv.FindElement(By.TagName("body")));

            if (!driver.FindElement(By.TagName("body")).Text.ToLower().Contains(text.ToLower()))
                throw new Exception(text + " doesn't contains in " + driver.Url);
        }
        public static void FullLoad(IWebDriver driver)
        {
            IWait<IWebDriver> wait = new OpenQA.Selenium.Support.UI.WebDriverWait(driver, TimeSpan.FromSeconds(30.00));
            wait.Until(driver1 => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
        }
    }
}
