﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Tests.Helpers
{
    internal class Constants
    {
        public const int N = 10;
        public static IWebDriver Driver
        {
            get
            {
                var WorkDir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                ChromeOptions ops = new ChromeOptions();
                ops.AddArguments("--disable-notifications");
                ops.AddExtensions(WorkDir + "/extension_10_10_2_0.crx");
                //ops.AddArguments("load-extension =${ " + WorkDir + "\\extension_10_10_2_0.crx" + "}");
                return new ChromeDriver(ops);
            }
        }


        #region Int Constants
        public const int LongTimeout = 3000;
        public const int ShortTimeout = 500;
        public const int Timeout1 = 20;
        public const int Timeout2 = 10;
        public const int TimeoutAfterClick = 1000;
        public const int TimeoutAssert = 500;
        public const int TimeoutForTimeSpan = 30;
        
        #endregion
    }
}
