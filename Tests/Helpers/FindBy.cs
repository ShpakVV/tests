﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Tests.Helpers;

namespace Tests.Helper
{
    public class FindBy
    {
        public static IWebElement Selector(By selector, IWebDriver driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(Constants.Timeout1));
            try
            {
                Thread.Sleep(Constants.ShortTimeout);
                wait.Until(drv => drv.FindElement(selector));
            }
            catch (Exception)
            { throw new Exception("FindBySelector " + " " + selector.ToString() + " not found"); }
            return wait.Until(drv => drv.FindElement(selector));
        }
        public static IWebElement XPath(string element, IWebDriver driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(Constants.Timeout1));
            try
            {
                Thread.Sleep(Constants.ShortTimeout);
                wait.Until(drv => drv.FindElement(By.XPath(element)));
            }
            catch (Exception)
            { throw new Exception("FindBySelector " + " " + element + " not found"); }
            return wait.Until(drv => drv.FindElement(By.XPath(element)));
        }
        public static IWebElement Id(string element, IWebDriver driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(Constants.Timeout1));
            try
            {
                Thread.Sleep(Constants.ShortTimeout);
                wait.Until(drv => drv.FindElement(By.Id(element)));
            }
            catch (Exception)
            { throw new Exception("FindBySelector " + " " + element + " not found"); }
            return wait.Until(drv => drv.FindElement(By.Id(element)));
        }
    }
}
